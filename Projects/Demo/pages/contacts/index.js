import React, { Component } from 'react'

export class Index extends Component {
    list=[]
    constructor(props) {
        super(props)
    
        this.state = {
             userInput:"",
        }
    }
    updateText = (event)=>{
        this.setState({
            userInput: event.target.value
        })
    }
    clicked = ()=>{
        this.list.push(this.state.userInput)
    }
    showList = () =>{

    }
    render() {
        return (
            <div>
                <input type="text" value={this.state.userInput} onChange={event=>this.updateText(event)}></input>
                <button onClick={this.clicked}>Add Country</button>
                <p>{this.list.length}</p>
                <button onClick={this.showlist}>Show List</button>
                {this.list.map(item=> 
                <p>
                    {item}
                </p>
                )}
            </div>
        )
    }
}

export default Index

