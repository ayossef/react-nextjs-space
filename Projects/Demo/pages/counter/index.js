import React, { Component } from 'react'

export default class Index extends Component {

    list = [1,2,2,2,2,2,2,2,2,9]
    constructor(props) {
        super(props);
        this.state = {
            counter: 30
        }
    }
    
    increment = () =>{
        this.setState({
            counter: this.state.counter+1
        })
    }

    render() {
        return (
            <div>
                <h3> {this.state.counter} </h3>
                <button onClick={this.increment}> Inc </button>
                <ul>
                {this.list.map(item => 
                    <li>{item}</li>
                )}
                </ul>
            </div>
        )
    }
}
