import Head from 'next/head'
import Image from 'next/image'
import styles from '../../styles/Home.module.css'

export default function Login() {
  return (
    <div className={styles.container}>
        <h1>
            Login Page
        </h1>
      <form>
          <input type="email" placeholder='Email here'></input>
          <input type="password" placeholder='*****'></input>
          <input type="submit" value="Login"></input>
      </form>
    </div>
  )
}
