import Head from 'next/head'
import Image from 'next/image'
import styles from '../../styles/Home.module.css'
import Login from './login'
import Register from './register'

export default function Main() {
  return (
    <div>
        <Login></Login>
        <Register></Register>
    </div>
  )
}
