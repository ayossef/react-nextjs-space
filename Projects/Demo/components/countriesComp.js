import React, { Component } from 'react'

export default class CountriesComp extends Component {
    constructor(){
        super()
        this.state = {
            list : ["UK", "US", "ZA", "EG", "AE"],
            userInput: ""
        }
    }
    textChanged = (ev) =>{
        this.setState({
            userInput: ev.target.value
        })
    }
    addCountry = ()=>{
        let currentCountry = this.state.userInput
        let currentList = this.state.list
        currentList.push(currentCountry)
        this.setState({
            list: currentList
        })
    }
    render() {
        return (
            <div>
                <h1>Numer of Countries: {this.state.list.length}</h1>
                <ul>
                    {this.state.list.map(item => 
                        <li>
                            {item}
                        </li>
                        )
                    }
                </ul>
                <input type="text" placeholder='Enter Country Name' onChange={ev=>this.textChanged(ev)}></input>
                <button value="Add Country" onClick={this.addCountry}>Add Country</button>
                <h6>{this.state.userInput}</h6>
            </div>
        )
    }
}
