import React from 'react'
import { useState } from 'react'
const url = 'https://jsonplaceholder.typicode.com/users/';
export default function NetworkComp() {
    const [user,stateUpdater] = useState({})
    const [currentUserIndex, setcurrentUserIndex] = useState(0)

    const clickActionPromiseThen = ()=>{
        updateCurrentUserIndex(currentUserIndex+1)
        let respPromise = fetch(url+currentUserIndex)
        respPromise.then(resp=>respParse(resp))
    }

    const respParse= (resp)=>{
        resp.json().then(data => 
            stateUpdater(data))
    }


    const clickActionAsyncAwait = async () =>{
        updateCurrentUserIndex(currentUserIndex+1)
        const resp =  await fetch(url+currentUserIndex)
        const data = await resp.json()
        stateUpdater(data)
    }

    const updateCurrentUserIndex = (newIndex)=>{
        if(newIndex > 10){
            newIndex = 1
        }
        setcurrentUserIndex(newIndex)
    }
 
    return (
        <div>
            <button onClick={clickActionPromiseThen}>Load Data</button>
            <br/>
            {user.id} <br/>
            {user.name} <br/>
            {user.email} <br/>
            {user.phone}
        </div>
    )
}
