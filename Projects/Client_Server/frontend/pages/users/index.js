import React from 'react'
import { useState } from 'react'
import UsersTable from '../../component/usersTable'
import AddUser from '../../component/addUser'

export default function index() {
    const url = "https://jsonplaceholder.typicode.com/users"
    const [usersList, setUsersList] = useState([])


    const loadData = async() =>{
        setUsersList(await(await fetch(url)).json())
        console.log(usersList)
    }
    return (
        <div>
            <button onClick={loadData}> Load Data </button>
            <UsersTable list={usersList}></UsersTable>
            <AddUser></AddUser>
        </div>
    )
}
