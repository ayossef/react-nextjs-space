import React from 'react'

export default function Details(props) {
    console.log(props)
    return (
        <div>
            <p>id: <br/> {props.user.id}</p>
            <p>Email: <br/> {props.user.email}</p>
            <p>Name: <br/> {props.user.name}</p>
        </div>
    )
}
