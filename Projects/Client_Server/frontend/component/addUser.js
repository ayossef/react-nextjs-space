import React from "react";
import { useState } from "react";

export default function AddUser() {
    let url = "http://localhost:3300/users/"

    const [user, setUser] = useState({})
    const addUser = async() =>{
        await fetch(url, {
            method:"POST",
            headers:{
                'Content-type':'application/json',
            },
            body: JSON.stringify(user)
        })
        setUser()
    }
    // const emailChanged = (evt) =>{
    //     let newUser = user
    //     newUser.email = evt.target.value
    //     setUser(newUser)
    // }
    // const nameChanged = (evt) =>{
    //     setUser({
    //         name: evt.target.value,
    //         email: user.email
    //     })
    // }
    const paramChanged = (evt)=>{
        let currentUser = user
        switch (evt.id) {
            case "name":
                currentUser.name = evt.target.value
                break;
            case "email":
                currentUser.email = evt.target.value
            default:
                break;
        }
        setUser(currentUser)
    }
  return (
    <div>
      <input  type="text" placeholder="Enter your name" onChange={evt=>paramChanged(evt)} id="name"></input><br/>
      <input type="email" value={user.email} placeholder="Enter your email" onChange={evt=>paramChanged(evt)} id="email"></input><br/>
      <button onClick={addUser}> Create User </button>
    </div>
  );
}
