import React from 'react'
import Link from 'next/link'
import {useState} from 'react'
import Details from './details'
export default function UsersTable(props) {
    let url = "http://localhost:3300/users/"
    let usersList = props.list
    // usersList = []
    const [editingEnabled, setEditingEnabled] = useState(true)
    const [userDetails, setUserDetails] = useState({name:'',email:'',id:0})

    const deleteUser = (userId) =>{
        // submit delete request to the api + userid
        fetch(url+userId, {
            method: "DELETE"
        })
    }
    const toggleEdit = () => {
        setEditingEnabled(!editingEnabled)
    }
    const showDetails=(id)=>{
        setUserDetails({
            id: usersList[id].id,
            name: usersList[id].name,
            email: usersList[id].email
        })
    }
    return (
        <div>
            <button onClick={toggleEdit}>Toggle Editing</button>
            <table>
                <th>
                    id
                </th>
                <th>
                    name
                </th>
                <th>
                    email
                </th>
                <th>
                    Details
                </th>
                {editingEnabled? <th>delete</th> : <></>}
                {usersList.map(user =>
                    <tr>
                        <td> {user.id} </td>
                        <td> {user.name} </td>
                        <td> {user.email}</td>
                        <td> <button onClick={()=>{showDetails(user.id)}}>Show detail</button></td>
                        {editingEnabled?
                        <td> <button onClick={()=>{deleteUser(user.id)}}>Delete</button></td>
                        : <></>
                    }
                    </tr>)}
            </table>
            <Details user={userDetails}></Details>
        </div>
    )
}
